const googleMap = function (map_id, locations) {

  let labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let labelIndex = 0;
  let i = 0;

  let map = new google.maps.Map(document.getElementById(map_id), {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    maxZoom: 17
  });
  let bounds = new google.maps.LatLngBounds();

  for (i = 0; i < locations.length; i++) {
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      label: labels[labelIndex++ % labels.length],
      map: map
    });
    bounds.extend(marker.position);
  }

  map.fitBounds(bounds);
};
