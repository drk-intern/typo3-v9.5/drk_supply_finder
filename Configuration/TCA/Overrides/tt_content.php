<?php

// SupplyFinder Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_supply_finder',
    'SupplyFinder',
    'LLL:EXT:drk_supply_finder/Resources/Private/Language/locallang_be.xlf:tt_content.drk_supplyfinder_plugin.title',
    'EXT:drk_supply_finder/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Angebotsfinder'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_supply_finder/Configuration/FlexForms/supplyfinder.xml',
    // ctype
    'drksupplyfinder_supplyfinder'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);

// SupplyFinderResult Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_supply_finder',
    'SupplyFinderResult',
    'LLL:EXT:drk_supply_finder/Resources/Private/Language/locallang_be.xlf:tt_content.drk_supplyfinderresult_plugin.title',
    'EXT:drk_supply_finder/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Angebotsfinder'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_supply_finder/Configuration/FlexForms/supplyfinderresult.xml',
    // ctype
    'drksupplyfinder_supplyfinderresult'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);

// SupplyFinderTagcloudPlugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_supply_finder',
    'SupplyFinderTagcloud',
    'LLL:EXT:drk_supply_finder/Resources/Private/Language/locallang_be.xlf:tt_content.drk_supplyfindertagcloud_plugin.title',
    'EXT:drk_supply_finder/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Angebotsfinder'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_supply_finder/Configuration/FlexForms/supplyfindertagcloud.xml',
    // ctype
    'drksupplyfinder_supplyfindertagcloud'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);
