<?php

namespace DRK\DrkSupplyFinder\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class SingleResultViewHelper extends AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArguments([
            ['url', 'string', 'the url to open in a new window', true, null],
        ]);
    }

    /**
     * @param array $arguments
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    protected function registerArguments(array $arguments)
    {
        if (is_array($arguments)) {
            foreach ($arguments as $argument) {
                if (is_array($argument)) {
                    $this->registerArgument($argument[0], $argument[1], $argument[2], $argument[3], $argument[4]);
                }
            }
        }
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
     */
    public function render()
    {
        $html = '';

        $url = $this->arguments['url'];
        if (!empty($url)) {
            $html .= '<script>window.open("' . $url . '", "_self")</script>';
        }

        return $html;
    }
}
