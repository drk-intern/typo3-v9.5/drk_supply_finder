<?php

namespace DRK\DrkSupplyFinder\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class OpenStreetMapViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    protected $settings;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Initialize arguments
     */
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArguments([
            ['locations', 'string', 'The locations to show on the map', true, null],
            ['latitudeField', 'string', 'The identifier for latitude', false, 'Latitude'],
            ['longitudeField', 'string', 'The identifier for longitude', false, 'Longitude'],
            ['height', 'string', 'The height', false, 440],
            ['width', 'string', 'The width', false, null],
            ['class', 'string', 'The class', false, null],
            ['alt', 'string', 'The alt', false, null],
            ['title', 'string', 'The title', false, null]
        ]);
    }

    /**
     * @param array $arguments
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    protected function registerArguments(array $arguments)
    {
        if (is_array($arguments)) {
            foreach ($arguments as $argument) {
                if (is_array($argument)) {
                    $this->registerArgument($argument[0], $argument[1], $argument[2], $argument[3], $argument[4]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
     */
    public function render()
    {
        $this->setSettings($this->templateVariableContainer->get('settings'));

        $this->assetCollector->addStyleSheet(
            'drk-supply-finder-osm-css',
            'EXT:drk_supply_finder/Resources/Public/Css/osm.css'
        );

        $this->assetCollector->addStyleSheet(
            'drk-supply-finder-osm-theme-css',
            'EXT:drk_supply_finder/Resources/Public/Scripts/OpenLayers-2.13.1/theme/default/style.css'
        );

        $this->assetCollector->addJavaScript(
            'drk-supply-finder-modal-js',
            'EXT:drk_supply_finder/Resources/Public/Scripts/jquery.modal.min.js',
            [],
            ['priority' => false]
        );

        $this->assetCollector->addJavaScript(
            'drk-supply-finder-autocomplete-js',
            'EXT:drk_supply_finder/Resources/Public/Scripts/jquery.autocomplete.min.js',
            [],
            ['priority' => false]
        );

        $id = 'map'.rand(1000,20000);
        $location = [];

        $mapHtml = '';
        $mapWidthStyleString = (int)$this->arguments['width'] > 0 ? 'width:' . (int)$this->arguments['width'] . 'px;' : '';
        $mapHeightStyleString = (int)$this->arguments['height'] > 0 ? 'height:' . (int)$this->arguments['height'] . 'px;' : '';

        if (!empty($this->arguments['locations'])) {
            $mapHtml = '<div id="' . $id . '" style="' . $mapHeightStyleString . $mapWidthStyleString . '">';
            if ($this->settings['GDPR']) {
                $mapHtml .= '<div class="osm_consent">
                <div><input class="button o-btn osm_gdpr" type="button" value="Karte anzeigen"></div>
                <div>Wenn Sie die Karte nutzen, werden einzelne Nutzerdaten an OpenStreetMap übertragen. Mit der Nutzung der Karte erklären Sie sich automatisch damit einverstanden.</div>
            </div>';
            }

            $mapHtml .= '</div>';

            $locationArray = [];
            $lastMarkerLabelArray = [];

            foreach ($this->arguments['locations'] as $location) {
                if (in_array($location['markerLabel'], $lastMarkerLabelArray)) {
                    continue;
                }
                $locationArray[] = '[' .
                    '"",' .
                    $location[$this->arguments['latitudeField']] . ',' .
                    $location[$this->arguments['longitudeField']] . ',' .
                    '"' . $location['markerLabel'] . '"' .
                    ']';
                $lastMarkerLabelArray[$location['markerLabel']] = $location['markerLabel'];
            }
            $locations = implode(',', $locationArray);

            $this->assetCollector->addJavaScript(
                'drk-supply-finder-osm-lib',
                'EXT:drk_supply_finder/Resources/Public/Scripts/OpenLayers-2.13.1/OpenLayers.js',
                [],
                ['priority' => true]
            );

            $this->assetCollector->addJavaScript(
                'drk-supply-finder-osm-js',
                'EXT:drk_supply_finder/Resources/Public/Scripts/osm_map.js',
                [],
                ['priority' => true]
            );


            $mapHtml .= "<script>var mapMarkerImg = '" . PathUtility::getPublicResourceWebPath (
                    'EXT:drk_supply_finder/Resources/Public/Images/map-marker-32.png') . "'</script>";
            if ($this->settings['GDPR']) {
                $mapHtml .= "<script>
                            var element = document.getElementById('{$id}');
                            element.addEventListener('click', function() {
                                    $('#{$id} .osm_consent').hide();
                                    osmMap('{$id}', [{$locations}])
                                   });

                        </script>";

            } else {

                $mapHtml .= "<script>setTimeout( osmMap('{$id}', [{$locations}]), 100)</script>";
            }
        }
        return $mapHtml;
    }
}
