<?php
namespace DRK\DrkSupplyFinder\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_supply-search-SwitchableControlleractionUpdater')]
class SwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drksupplyfinder_supplyfinder',
            'switchableControllerActions' => 'SupplyFinder->showForm;SupplyFinder->showOfferResult;SupplyFinder->showAddressResult;SupplyFinder->showOfferDetail',
            'targetListType' => '',
            'targetCtype' => 'drksupplyfinder_supplyfinder'
        ], [
            'sourceListType' => 'drksupplyfinder_supplyfinder',
            'switchableControllerActions' => 'SupplyFinder->showOfferResult;SupplyFinder->showAddressResult;SupplyFinder->showOfferDetail',
            'targetListType' => '',
            'targetCtype' => 'drksupplyfinder_supplyfinderresult'
        ], [
            'sourceListType' => 'drksupplyfinder_supplyfinder',
            'switchableControllerActions' => 'SupplyFinder->showTagCloud;SupplyFinder->showOfferResult;SupplyFinder->showOfferDetail',
            'targetListType' => '',
            'targetCtype' => 'drksupplyfinder_supplyfindertagcloud'
        ],
    ];
}
