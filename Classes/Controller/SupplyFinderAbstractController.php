<?php

namespace DRK\DrkSupplyFinder\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkSupplyFinder\Domain\Repository\SupplyFinderRepository;
use Exception;
use TYPO3\CMS\Core\Page\AssetCollector;
use http\Exception\BadUrlException;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFileWritePermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InvalidFileException;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * SupplyFinderAbstractController
 */
class SupplyFinderAbstractController extends AbstractDrkController
{

    /**
     * @var SupplyFinderRepository $supplyFinderRepository
     */
    protected $supplyFinderRepository;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        private readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Init
     *
     * @return void
     * @throws InvalidFileException
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        if ($this->settings['show_detail']) {
            $this->assetCollector->addStyleSheet(
                'supplyfinder-modal',
                'EXT:drk_supply_finder/Resources/Public/Css/jquery.modal.min.css'
            );
        }

        $this->assetCollector->addStyleSheet(
            'supplyfinder-styles',
            'EXT:drk_supply_finder/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'supplyfinder-javascript',
            'EXT:drk_supply_finder/Resources/Public/Scripts/tx_drksupplyfinder.js',
            [],
            ['priority' => false] // lädt in footer

        );

        if (!empty($this->settings['autocompleteSrc']) && intval($this->settings['autocompleteUse']) == 1) {

            $location_json = $this->getLocationFilePath();
            $javaScript = "
                $.getScript( '".PathUtility::getPublicResourceWebPath (
                    'EXT:drk_supply_finder/Resources/Public/Scripts/jquery.autocomplete.min.js') . "' " .
                ", function () {
                  'use strict';
                    $.getJSON('$location_json', function(data) {

                        $('#tx-supplyfinder-searchbox').autocomplete({
                          lookup: data,
                          minChars: 1,
                          showNoSuggestionNotice: false,
                        });

                        $('#tx-supplyfinder-adressen-searchbox').autocomplete({
                            lookup: data,
                            minChars: 1,
                            showNoSuggestionNotice: false,
                          });

                    });
                });
            ";

            $this->assetCollector->addInlineJavaScript(
                'supplyfinder-autocomplete',
                $javaScript,
                [],
                ['priority' => false] // lädt in footer


            );
        }
    }

    /**
     * @param SupplyFinderRepository $supplyFinderRepository
     */
    public function injectSupplyFinderRepository(SupplyFinderRepository $supplyFinderRepository): void
    {
        $this->supplyFinderRepository = $supplyFinderRepository;
    }

    /**
     *
     * error_reporting
     *
     * @throws Exception
     */
    protected function error_reporting(): void
    {
        $errors = array_merge($this->error, $this->supplyFinderRepository->getErrors());
        $this->view->assign('errorMessages', $errors);
    }

    /**
     * @param int $preselectedOfferId
     * @return void
     */
    protected function assignOfferList(int $preselectedOfferId = 0): void
    {
        $preselection = [];
        $offerList = [];

        /**
         * List of possible list as array of selectable offer ids
         * plugin.tx_drksupplyfinder.settings.offerIds {
         *    10000 = Alten- und Pflegeheime
         *    6 = Ambulante Pflege
         *    5 = Hausnotruf
         *    340000 =  Kleidercontainer
         *    9 = Menübringdienst
         *    75 = Rotkreuzkurs Erste Hilfe
         * }
         */

        if (isset($this->settings['offerIds']) && is_array($this->settings['offerIds'])
            && !empty($this->settings['offerIds'])) {
             foreach ($this->settings['offerIds'] as $offerId => $offerName) {
                 $offerList[$offerId]['orgOfferId'] = $offerId;
                 $offerList[$offerId]['orgOfferDescription'] = $offerName;
                 $offerList[$offerId]['orgOfferClass'] = '';
            }
        }
        else {
            $offerList = $this->supplyFinderRepository->findOfferList();
        }

        if (!empty($preselectedOfferId)) {
            foreach ($offerList as $offer) {
                if ($offer['orgOfferId'] === (int)$preselectedOfferId) {
                    $preselection = $offer;
                    break;
                }
            }
        }

        $this->view->assignMultiple([
            'offerList' => $offerList,
            'preselection' => $preselection,
        ]);
    }

    /**
     * @param int $OfferTypId
     * @return string
     */
    protected function getOfferTypById(int $OfferTypId = 0): string
    {
        $offerTypList = $this->supplyFinderRepository->findOfferList();
        $OfferTypName = '';
        if ($OfferTypId > 0) {
            foreach ($offerTypList as $offerTyp) {
                if ($offerTyp['orgOfferId'] === (int)$OfferTypId) {
                    $OfferTypName = $offerTyp['orgOfferDescription'];
                    break;
                }
            }
        }

        return $OfferTypName;
    }

    /**
     * @return string
     * @throws InsufficientFileWritePermissionsException|BadUrlException|ResourceDoesNotExistException
     */
    protected function getLocationFilePath(): string
    {
        $publicPath = Environment::getPublicPath();
        $jsonFile = $jsonFileContent = '';
        $src = $this->settings['autocompleteSrc'];

        $typo3tempDirectory = '/typo3temp/'  . $this->request->getControllerExtensionKey() . '/';
        GeneralUtility::mkdir_deep($publicPath . $typo3tempDirectory);
        $jsonFile = $typo3tempDirectory . 'city_list.json';

        if (file_exists($jsonFile)) {
            return $jsonFile;
        }
        else {

            if ($jsonFileContent = GeneralUtility::getUrl($src)) {
                if (!GeneralUtility::writeFile($publicPath . $jsonFile, $jsonFileContent, true)) {
                    throw new InsufficientFileWritePermissionsException('Autocomplete: Temporary file could not be created!');
                }
                else {
                    return $jsonFile;
                }

            }
            else {
                throw new ResourceDoesNotExistException('File could not be found! Wrong "autocompleteSrc"');
            }
        }
    }

    /**
     * Add JavaScript tracking if necessary
     */
    protected function addTrackerJS(): void
    {
        if (empty($this->settings['use_tracker'])
            || empty($this->settings['tracker_path'])
        ) {
            return;
        }

        $javaScript =
            '   /**
                 * DRK Tracker
                 */
                var DRKTracker = {
                    init: function(options) {
                        this.options = $.extend({
                            trackerPath:  \'\'
                        }, options || {});
                    },

                    track: function(trackerParameter){

                        if ( trackerParameter != \'\' ) {
                            $.get (this.trackerPath + "track/" + trackerParameter);
                        }
                    },
                };
                DRKTracker.trackerPath=' . GeneralUtility::quoteJSvalue($this->settings['tracker_path']) . ';
            ';

        $this->assetCollector->addInlineJavaScript(
            'drk-supply-tracker',
            $javaScript,
            [],
            ['priority' => false]
        );

    }
    /**
     * @param string $strCsv
     * @return array
     */
    protected function explodeRangeCsv( string $strCsv = ""): array
    {
        $result = [];

        if (empty($strCsv)) {
            return $result;
        }

        $arrayCsv = explode(',', $strCsv);

        foreach ($arrayCsv as $value) {
            $result[$value] = $value . 'km';
        }

        return $result;

    }
}
