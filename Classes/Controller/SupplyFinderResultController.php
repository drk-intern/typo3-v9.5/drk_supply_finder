<?php

namespace DRK\DrkSupplyFinder\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use Psr\Http\Message\ResponseInterface;
use GeorgRinger\NumberedPagination\NumberedPagination;
use TYPO3\CMS\Core\Pagination\ArrayPaginator;

/**
 * SupplyFinderResultController
 */
class SupplyFinderResultController extends SupplyFinderAbstractController
{

    /**
     * showOfferResultAction
     *
     * @param string $zip
     * @param int $offerId
     * @param int $iSearchRange
     * @return ResponseInterface
     * @throws Exception
     */
    public function showOfferResultAction(string $zip = '', int $offerId = 0, int $iSearchRange = 0): ResponseInterface
    {
        $this->addTrackerJS();

        $organisation = [];
        $isSearchRangeSet = $iSearchRange > 0;

        $organisationLimitation = empty($this->settings['organisation_limitation']) ? '' : $this->settings['organisation_limitation'];
        $zipLengthError = (strlen($zip) < 3);
        $currentPage = $this->request->hasArgument('currentPage') ? (int)$this->request->getArgument('currentPage') : 1;

        // if $iSearchRange is empty, set initial range 5km
        $iSearchRange = $iSearchRange == 0 ? 5 : $iSearchRange;

        /**
         * check for valid zip code
         */
        if ($zipLengthError) {
            $this->assignOfferList($offerId);
            $this->view->assignMultiple([
                'zip' => $zip,
                'offerId' => $offerId,
                'organisation' => [],
                'zipLengthError' => true,
            ]);

            return $this->htmlResponse();
        }

        $offerId = $offerId ?: '9999';
        $offerLinks = $this->supplyFinderRepository->findOfferLinksByZipOrCity($offerId, $zip, $iSearchRange, $organisationLimitation);

        // increase search_range with value from settings
        if (empty($offerLinks) && !$isSearchRangeSet) {
            $offerLinks = $this->supplyFinderRepository->findOfferLinksByZipOrCity(
                $offerId,
                $zip,
                intval($this->settings['search_range']),
                $organisationLimitation
            );
        }

        // fallback if no offerlinks
        if (empty($offerLinks)) {
            $org = $this->supplyFinderRepository->findByZipOrCity($zip, 'k');
            $organisation = array_shift($org);
        }

        $this->assignOfferList($offerId);
        $this->view->assignMultiple([
            'zip' => $zip,
            'offerId' => $offerId,
            'offerLinks' => $offerLinks,
            'organisation' => $organisation,
            'zipLengthError' => false,
        ]);

        /**
         * Set pagination
         */
        $itemsPerPage = 10;
        $maximumLinks = 15;

        $paginator = new ArrayPaginator($offerLinks, $currentPage, $itemsPerPage);
        $pagination = new NumberedPagination($paginator, $maximumLinks);
        $this->view->assign('pagination', [
            'paginator' => $paginator,
            'pagination' => $pagination,
        ]);

        // select search range
        if (isset($this->settings['search_range_selection']) && !empty($this->settings['search_range_selection']))
        {
            $this->view->assign('searchRangeSelection', $this->explodeRangeCsv($this->settings['search_range_selection']));
        } else {
            $this->view->assign('searchRangeSelection', []);
        }

        $this->view->assign('searchRange', $iSearchRange);

        $this->view->assign('error', false);
        // if Error, then show it now
        if (!empty($this->error)) {
            $this->view->assign('error', true);
            $this->error_reporting();
        }

        return $this->htmlResponse();
    }

    /**
     *
     * showOfferDetailAction
     * @param int $offerRecordId
     * @return ResponseInterface
     * @throws Exception
     */
    public function showOfferDetailAction(int $offerRecordId = 0): ResponseInterface
    {

        if (!empty($offerRecordId)) {
            $offerDetail = $this->supplyFinderRepository->getOfferDetail ($offerRecordId);
        }

        $this->view->assign('offerDetail', $offerDetail ?? []);

        // if Error, then show it now
        if (!empty($this->error)) {
            $this->view->assign('error', true);
            $this->error_reporting();
        } else {
            $this->view->assign('error', false);
        }

        return $this->htmlResponse();
    }

    /**
     * @param string $zip
     * @return ResponseInterface
     * @throws Exception
     */
    public function showAddressResultAction(string $zip = ''): ResponseInterface
    {
        $this->addTrackerJS();

        $organisations = [];
        $zipLengthError = (strlen($zip) < 3);
        $currentPage = $this->request->hasArgument('currentPage') ? (int)$this->request->getArgument('currentPage') : 1;

        if (!$zipLengthError) {
            $organisations = $this->supplyFinderRepository->findByZipOrCity($zip);
        }

        // set Latitude and Longitude for MapHelper
        if (!empty($organisations) && is_array($organisations)) {
            foreach ($organisations as $iKey => $aOrg) {
                key_exists(
                    'orgLatitude',
                    $aOrg
                ) ? $organisations[$iKey]['Latitude'] = $organisations[$iKey]['orgLatitude'] : '';
                key_exists(
                    'orgLongitude',
                    $aOrg
                ) ? $organisations[$iKey]['Longitude'] = $organisations[$iKey]['orgLongitude'] : '';
            }
        }

        $this->view->assignMultiple([
            'organisations' => $organisations,
            'zip' => $zip,
            'zipLengthError' => $zipLengthError,
        ]);

        /**
         * Set pagination
         */
        $itemsPerPage = 10;
        $maximumLinks = 15;

        $paginator = new ArrayPaginator($organisations, $currentPage, $itemsPerPage);
        $pagination = new NumberedPagination($paginator, $maximumLinks);
        $this->view->assign('pagination', [
            'paginator' => $paginator,
            'pagination' => $pagination,
        ]);

        return $this->htmlResponse();

    }
}
