<?php

namespace DRK\DrkSupplyFinder\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * SupplyFinderTagcloudController
 */
class SupplyFinderTagcloudController extends SupplyFinderAbstractController
{
    /**
     * showTagCloudAction
     *
     * @return ResponseInterface
     * @throws Exception
     */
    public function showTagCloudAction(): ResponseInterface
    {
        // prepare tags
        if (!empty($this->settings['taglist']) && is_string($this->settings['taglist']))
        {
            $aTagList = [];
            $aTagListRaw = explode(',', $this->settings['taglist'], 100);

            // Cleanup search words
            foreach ($aTagListRaw as  $aTag) {
                if (!empty($aTag)) {
                    $aTagList[] = trim(substr(str_replace(['#', ';', '"', "'", '>', '<', '{', '}', '[', ']'], '',
                        $aTag), 0, 32));
                }
            }

            if (empty($aTagList)) {
                $this->error['Fehler'] = 'Die Liste der Suchbegriffen ist leer!';
            }  else {
                $this->view->assign('tagList',$aTagList);
            }
        }
        else {
            $this->error['Fehler'] = 'Bitte eine Liste an Suchbegriffen hinterlegen!';
        }

        // get offer description
        if (intval($this->settings['preselection']) > 0) {
            $this->view->assign('offerTypDescription',$this->getOfferTypById(intval($this->settings['preselection'])));
        }
        else
        {
            $this->error['Fehler'] = 'Es wurde kein Angebot ausgewählt!';
        }



        // if Error, then show it now
        if (!empty($this->error)) {
            $this->view->assign('error', true);
            $this->error_reporting();
        } else {
            $this->view->assign('error', false);
        }

        return $this->htmlResponse();
    }
}
