<?php

namespace DRK\DrkSupplyFinder\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;

/**
 * SupplyFinderRepository
 */
class SupplyFinderRepository extends AbstractDrkRepository
{

    /**
     * @param string $sword
     * @param string $stypfilter : stypfilter can be empty for all, l, k, s or g
     * @return array
     * @throws \Exception
     */
    public function findByZipOrCity($sword, $stypfilter = '')
    {
        $cacheIdentifier = sha1(json_encode(['drk_supply_finder|findByZipOrCity', $sword, $stypfilter]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }


        $client = $this->getDldbClient();
        if ($client) {
            $organisations = $offerLinks = Utility::convertObjectToArray(
                $client->getOrganisationbyZiporCity($sword, 'typ', 'asc')
            );
        }

        if (empty($organisations)) {
            return [];
        }

        $latLngArray = [];
        $latLngCounter = 0;
        $iteration = 0;

        $_atypfilter = ['l', 'k', 's'];
        if ($stypfilter != '' || array_key_exists(strtolower($stypfilter), $_atypfilter)) {
            $key = array_search(strtoupper($stypfilter), array_column($organisations, 'orgType'));
            $aOrganisations[$key] = $organisations[$key];
        } else {
            $aOrganisations = $organisations;
        }

        foreach ($aOrganisations as &$aOrganisation) {
            $orgGeoHash = $aOrganisation['orgLatitude'] . ':' . $aOrganisation['orgLongitude'];
            if (($latLngIndex = array_search($orgGeoHash, $latLngArray)) !== false) {
                $currentAsciiChar = $latLngIndex;
            } else {
                $latLngArray[] = $orgGeoHash;
                $currentAsciiChar = $latLngCounter++;
            }

            $aOrganisation['Latitude'] = $aOrganisation['orgLatitude'];
            $aOrganisation['Longitude'] = $aOrganisation['orgLongitude'];

            $letter = chr($currentAsciiChar + 65);
            $aOrganisation['markerLabel'] = str_repeat($letter, $iteration);

            // At the end of the alphabet, start again from the beginning.
            if ($latLngCounter == 26) {
                $iteration ++;
                $latLngCounter = 0;
            }
        }

        $this->getCache()->set($cacheIdentifier, $aOrganisations, [], $this->long_cache);

        return $aOrganisations;
    }

    /**
     * @param string $sortBy
     * @param string $orderBy
     * @return array
     */
    public function findOfferList($sortBy = 'description', $orderBy = 'asc')
    {
        $cacheIdentifier = sha1(json_encode(['drk_supply_finder|findOfferList', $sortBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();

        if ($client) {
            $offers = $offerLinks = Utility::convertObjectToArray(
                $client->getOfferDescriptionList($sortBy, $orderBy)
            );
        }

        if (empty($offers)) {
            return [];
        }

        $this->getCache()->set($cacheIdentifier, $offers, [], $this->heavy_cache);

        return $offers;
    }

    /**
     * @param $offerId
     * @param $zip
     * @param int $range
     * @return array
     */
    public function findOfferLinksByZipOrCity($offerId, $zip, $range = 5, $organisationLimitation = '')
    {
        $cacheIdentifier = sha1(json_encode(['drk_supply_finder|findOfferLinksByZipOrCity', $offerId, $zip, $range, $organisationLimitation]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();
        if ($client) {
            $offerLinks = Utility::convertObjectToArray(
                $client->getOrganisationOfferLinksbyZiporCityTracked($zip, $offerId, $range, '', $organisationLimitation)
            );
        }

        if (empty($offerLinks)) {
            return [];
        }

        usort($offerLinks, function ($a, $b) {
            return ($a['distance'] == $b['distance']) ? 0 : (($a['distance'] < $b['distance']) ? -1 : 1);
        });

        $latLngArray = [];
        $latLngCounter = 0;
        $iteration = 1;

        foreach ($offerLinks as &$offerLink) {
            if (empty($offerLink['orgOfferName'])) {
                $offerLink['orgOfferName'] = $offerLink['orgOfferDescription'];
            }
            if (empty($offerLink['offerZipcode'])) {
                $offerLink['offerStreet'] = $offerLink['Organisation']['orgStreet'];
                $offerLink['offerZipcode'] = $offerLink['Organisation']['orgZipcode'];
                $offerLink['offerCity'] = $offerLink['Organisation']['orgCity'];
                $offerLink['orgOfferLatitude'] = $offerLink['Organisation']['orgLatitude'];
                $offerLink['orgOfferLongitude'] = $offerLink['Organisation']['orgLongitude'];
            }
            if (empty($offerLink['orgOfferURL'])) {
                $offerLink['orgOfferURL'] = $offerLink['orgOfferTypeURL'];
            }

            $offerGeoHash = $offerLink['orgOfferLatitude'] . ':' . $offerLink['orgOfferLongitude'];
            if (($latLngIndex = array_search($offerGeoHash, $latLngArray)) !== false) {
                $currentAsciiChar = $latLngIndex;
            } else {
                $latLngArray[] = $offerGeoHash;
                $currentAsciiChar = $latLngCounter++;
            }
            $letter = chr($currentAsciiChar + 65);

            $offerLink['Latitude'] = $offerLink['orgOfferLatitude'];
            $offerLink['Longitude'] = $offerLink['orgOfferLongitude'];
            $offerLink['markerLabel'] = str_repeat($letter, $iteration);

            // At the end of the alphabet, start again from the beginning.
            if ($latLngCounter == 26) {
                $iteration ++;
                $latLngCounter = 0;
            }
        }

        $this->getCache()->set($cacheIdentifier, $offerLinks);

        return $offerLinks;
    }

    /**
     * ItemsProcFunc for SupplyFinder->settings.flexforms.preselection
     *
     * @param array $parameters
     * @return array
     */
    public function addOfferListItems(array $parameters)
    {
        $this->initSettingsForListItems($parameters);
        foreach ($this->findOfferList() as $offer) {
            $parameters['items'][] = [
                $offer['orgOfferDescription'],
                $offer['orgOfferId'],
                '',
            ];
        }

        return $parameters;
    }

    /**
     * @param int $offerRecordId
     * @return false | getOrganisationOfferDetail
     * @throws \Exception
     */

    public function getOfferDetail (int $offerRecordId = 0) {

        $cacheIdentifier = sha1(json_encode(['drk_supply_finder|getOfferDetail', $offerRecordId]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        if (isset($this->settings['apiKey']) && !empty($this->settings['apiKey']))  {
            $ApiKey = $this->settings['apiKey'];
        }
        else {
            $this->error = ['Fehler' => 'API Schlüssel fehlt!'];
            return false;
        }

        if (empty($offerRecordId)) {
            $this->error = ['Fehler' => 'Fehler: Es wurde keine ID eines Angebotes übergeben.'];
            return false;
        }

        try {
            $soapClient = $this->getDldbClient();
            $result = $soapClient->getOrganisationOfferDetail($offerRecordId, $ApiKey);

            $this->getCache()->set($cacheIdentifier, $result, [], $this->heavy_cache);

            return $result;
        } catch (Exception $exception) {

            $this->error = ['Fehler' => $exception->getMessage()];
            return false;

        }

    }

}
