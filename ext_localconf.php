<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}
## SupplyFinder Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_supply_finder',
    'SupplyFinder',
    [
        \DRK\DrkSupplyFinder\Controller\SupplyFinderController::class => 'showForm',
    ],
    // non-cacheable actions
    [
        \DRK\DrkSupplyFinder\Controller\SupplyFinderController::class => '',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## SupplyFinderResult Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_supply_finder',
    'SupplyFinderResult',
    [
        \DRK\DrkSupplyFinder\Controller\SupplyFinderResultController::class => 'showOfferResult, showAddressResult, showOfferDetail',
    ],
    // non-cacheable actions
    [
        \DRK\DrkSupplyFinder\Controller\SupplyFinderResultController::class => 'showOfferResult, showAddressResult, showOfferDetail',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## SupplyFinderTagcloud Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_supply_finder',
    'SupplyFinderTagcloud',
    [
        \DRK\DrkSupplyFinder\Controller\SupplyFinderTagcloudController::class => 'showTagCloud',
    ],
    // non-cacheable actions
    [
        \DRK\DrkSupplyFinder\Controller\SupplyFinderTagcloudController::class => '',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']['drksupplyfinder']
    = 'DRK\DrkSupplyFinder\Hook\T3libTcemainHook->clearCachePostProc';
